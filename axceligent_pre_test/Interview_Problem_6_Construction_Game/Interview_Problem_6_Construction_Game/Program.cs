﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interview_Problem_6_Construction_Game
{
    class Program
    {
        static void Main(string[] args)
        {
            var myHouse = new Building()
                            .AddKitchen()
                            .AddBedroom("master")
                            .AddBedroom("guest")
                            .AddBalcony();
            myHouse.Build();

            //kitchen, master room, guest room, balcony
            Console.WriteLine(myHouse.Describe());

            myHouse.AddKitchen().AddBedroom("another");

            //before build the house description, still is as before
            //kitchen, master room, guest room, balcony
            Console.WriteLine(myHouse.Describe());
            myHouse.Build();

            //it only shows the kitchen after build
            //kitchen, master room, guest room, balcony, kitchen, another room
            Console.WriteLine(myHouse.Describe());
            Console.ReadLine();
        }

        public class HouseInfra
        {
            public string Kitchen { get; set; }
            public string BedRoom { get; set; }
            public string Balcony { get; set; }
        }

        public class HouseWorkDone
        {
            public string HouseDescription { get; set; }
        }

        public class Building : ObjectFactoryBase<HouseInfra>
        {
            private HouseWorkDone _workDone;
            public Building()
            {
                _workDone = new HouseWorkDone();
            }

            public Building AddKitchen()
            {
                Object = new HouseInfra()
                {
                    Kitchen = "kitchen"
                };
                ObjectList.Add(Object);
                return this;
            }

            public Building AddBedroom(string roomName)
            {
                Object = new HouseInfra()
                {
                    BedRoom = roomName + " room"
                };
                ObjectList.Add(Object);
                return this;
            }

            public Building AddBalcony()
            {
                Object = new HouseInfra()
                {
                    Balcony = "Balcony"
                };
                ObjectList.Add(Object);
                return this;
            }

            public string Describe()
            {
                return _workDone.HouseDescription;
            }

            public Building Build()
            {
                _workDone = new HouseWorkDone();
                foreach (var data in ObjectList)
                {
                    _workDone.HouseDescription += ", ";
                    _workDone.HouseDescription += data.Kitchen + data.BedRoom + data.Balcony;
                }
                _workDone.HouseDescription = _workDone.HouseDescription.Remove(0, 2);
                return this;
            }
        }

        public interface IObjectFactoryBase<TEntityT>
        {
            TEntityT Object { get; set; }
            List<TEntityT> ObjectList { get; set; }
        }

        public class ObjectFactoryBase<TEntityT> : IObjectFactoryBase<TEntityT>
        {
            public TEntityT Object { get; set; }
            public List<TEntityT> ObjectList { get; set; }

            public ObjectFactoryBase()
            {
                ObjectList = new List<TEntityT>();
            }
        }
    }
}
