﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interview_Problem_3_John_The_Robot
{
    class Program
    {
        static void Main(string[] args)
        {
            var john = new Humanoid(new Dancing());
            Console.WriteLine(john.ShowSkilll());

            var alex = new Humanoid(new Cooking());
            Console.WriteLine(alex.ShowSkilll());

            var bob = new Humanoid();
            Console.WriteLine(bob.ShowSkilll());

            Console.ReadLine();
        }
    }

    public interface IRobotBehaviour
    {
        string ShowSkill();
    }

    public class Dancing : IRobotBehaviour
    {
        public string ShowSkill()
        {
            return "dancing";
        }
    }

    public class Cooking : IRobotBehaviour
    {
        public string ShowSkill()
        {
            return "cooking";
        }
    }
    
    public class Humanoid
    {
        private readonly IRobotBehaviour _robotBehaviour;
        public Humanoid(IRobotBehaviour robotBehaviour)
        {
            _robotBehaviour = robotBehaviour;
        }

        public Humanoid()
        {

        }
        public string ShowSkilll()
        {
            if (_robotBehaviour == null) return "no skill is defined";
            return _robotBehaviour.ShowSkill();
        }
    }
}
