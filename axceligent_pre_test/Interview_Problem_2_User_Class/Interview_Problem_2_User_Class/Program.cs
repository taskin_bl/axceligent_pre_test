﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interview_Problem_2_User_Class
{
    public class Program
    {
        private static List<string> UserList { get; set; }

        static void Main(string[] args)
        {
            while (true)
            {
                var user = new User();
                Console.WriteLine("please enter the username, or q to exit");
                var userName = Console.ReadLine();
                if (userName == "q")
                {
                    break;
                }
                user.Add(userName);
                Console.WriteLine(@"number of addedUser " + user.GetUsersCount());
            }
        }

        public class User
        {
            public int GetUsersCount()
            {
                return UserList.Count;
            }

            public void Add(string userName)
            {
                if (UserList == null)
                    UserList = new List<string>();
                UserList.Add(userName);
            }
        }
    }
}
