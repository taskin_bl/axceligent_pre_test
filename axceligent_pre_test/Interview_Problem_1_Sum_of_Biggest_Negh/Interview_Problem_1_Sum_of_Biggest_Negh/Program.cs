﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interview_Problem_1_Sum_of_Biggest_Negh
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = Challenge(new int[] { 1, 6, 1, 2, 6, 1, 6, 6 });
            Console.WriteLine(result);
            Console.ReadLine();
        }

        public static int Challenge(int[] input)
        {
            Dictionary<int, int> dictionary = new Dictionary<int, int>();

            for (var index = 0; index < input.Length; index++)
            {
                if (dictionary.ContainsKey(input[index]))
                    dictionary[input[index]]++;
                else
                    dictionary[input[index]] = 1;
            }

            int maxVal = 0;
            int consecutiveMax = 0;
            int counter = 0;
            foreach (KeyValuePair<int, int> entry in dictionary)
            {
                if (entry.Value > maxVal)
                    maxVal = entry.Value;
            }

            var removalAry = input
                        .GroupBy(e => e)
                        .Where(e => e.Count() < maxVal - 1)
                        .Select(e => e.First()).ToArray();

            for (int index = 0; index < removalAry.Length; index++)
                input = input.Where(val => val != removalAry[index]).ToArray();

            while (counter <= input.Length - 2)
            {
                if (input[counter] + input[counter + 1] > consecutiveMax)
                {
                    consecutiveMax = input[counter] + input[counter + 1];
                }
                counter++;
            }

            return consecutiveMax;
        }
    } 
}
