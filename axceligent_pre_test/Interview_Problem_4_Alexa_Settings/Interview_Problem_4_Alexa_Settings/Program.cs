﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interview_Problem_4_Alexa_Settings
{
    public class Program
    {
        static void Main(string[] args)
        {
            var alexa = new Alexa();
            Console.WriteLine(alexa.Talk()); //print hello, i am Alexa
            alexa.Configure(x =>
            {
                x.GreetingMessage = "Hello {OwnerName}, I'm at your service";
                x.OwnerName = "Bob Marley";
            });
            Console.WriteLine(alexa.Talk()); //print Hello Bob Marley, I'm at your service
            Console.WriteLine("press any key to exit ...");
            Console.ReadKey();
        }

        public class Alexa
        {
            public delegate void ConfigureDelegate(Alexa alexa);
            public string GreetingMessage { get; set; }
            public string OwnerName { get; set; }

            public string Talk()
            {
                if (!string.IsNullOrEmpty(GreetingMessage) && !string.IsNullOrEmpty(OwnerName))
                    return GreetingMessage.Replace("{OwnerName}", OwnerName);
                return "hello, i am Alexa";
            }

            public void Configure(ConfigureDelegate configDelegate)
            {
                configDelegate(this);
            }
        }
    }
}
